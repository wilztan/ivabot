package main

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/heroku/x/hmetrics/onload"
	"github.com/line/line-bot-sdk-go/linebot"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/wilztan/wilztanx/src/model"
)

var bot *linebot.Client

func main() {
	port := os.Getenv("PORT")
	cst := os.Getenv("channel_secret")
	cat := os.Getenv("channel_access_token")
	var err error

	if port == "" {
		log.Fatal("$PORT must be set")
	}

	bot, err = linebot.New(cst, cat)
	if err != nil {
		panic(err)
	}

	router := gin.New()
	router.Use(gin.Logger())
	router.LoadHTMLGlob("templates/*.tmpl.html")
	router.Static("/static", "static")

	router.POST("/hook", handleWebHook)
	router.GET("/", handleTest)

	router.Run(":" + port)
}

func handleWebHook(c *gin.Context) {
	events, err := bot.ParseRequest(c.Request)

	if err != nil {
		if err == linebot.ErrInvalidSignature {
			c.JSON(500, gin.H{
				"message": err,
			})
		} else {
			c.JSON(500, gin.H{
				"message": err,
			})
		}
		return
	}

	for _, event := range events {
		if event.Type == linebot.EventTypeMessage {
			switch message := event.Message.(type) {
			case *linebot.TextMessage:
				//quota, err := bot.GetMessageQuota().Do()

				if strings.Contains(message.Text, ".blogspot.com") {
					if _, err = bot.ReplyMessage(
						event.ReplyToken, linebot.NewTextMessage("The source of the text contains link from unofficial source, I think you should check the content first :)"),
					).Do(); err != nil {
						log.Print(err)
					}
				}

				if !AnalyzeCall(message.Text) {
					return
				}

				var resultItem []model.ResultItem
				var detailInfo = "I've been reading your information, have you considered these information first?"
				var isValid = true

				if AnalyzeDeeperCall(message.Text) {
					err, isValid, resultItem = CallSearchCrawlerTextApi(message.Text)
					fmt.Println("Search")
				} else {
					err, isValid, resultItem = CallCrawlerTextApi(message.Text)
					fmt.Println("Crawl")
				}

				if !isValid {
					if _, err = bot.ReplyMessage(
						event.ReplyToken, linebot.NewTextMessage("I Could not find any issues regarding this reference, I don't think this is a valid event"),
					).Do(); err != nil {
						log.Print(err)
					}
				}

				container := GenerateTemplateMessage(detailInfo, resultItem)

				if err != nil {
					log.Println("Quota err:", err)
				}
				if _, err = bot.ReplyMessage(
					event.ReplyToken,
					linebot.NewFlexMessage("alt text", container),
				).Do(); err != nil {
					log.Print(err)
				}

			}
		}
	}
}

func handleTest(c *gin.Context) {
	c.HTML(http.StatusOK, "index.tmpl.html", nil)
}

func AnalyzeCall(text string) bool {
	lowerText := strings.ToLower(text)
	if strings.Contains(lowerText, "iva help ") {
		return true
	}
	return false
}

func AnalyzeDeeperCall(text string) bool {
	lowerText := strings.ToLower(text)
	if strings.Contains(lowerText, "iva help this ") {
		return true
	}
	return false
}

func ProcessText(text string) string {
	lowerText := strings.ToLower(text)
	var hideKeyWordText string
	if AnalyzeDeeperCall(text) {
		hideKeyWordText = strings.Replace(lowerText, "iva help this ", "", -1)
	} else {
		hideKeyWordText = strings.Replace(lowerText, "iva help ", "", -1)
	}
	hideKeyWordText = strings.Replace(hideKeyWordText, " ", "%20", -1)
	return hideKeyWordText
}

func CallCrawlerTextApi(text string) (err error, isValid bool, resultItem []model.ResultItem) {
	isValid = true

	url := "http://3.1.64.2/scrape?query=" + ProcessText(text)

	resp, err := http.Get(url)

	fmt.Printf(" URL IS %s ", url)

	if err != nil {
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var data = new(model.CrawlResult)
	err = json.Unmarshal(body, &data)
	if err != nil {
		fmt.Println("whoops:", err)
	}
	fmt.Printf("HMMM %v", data)

	if len(data.Results) < 1 {
		isValid = false
		return
	}

	for i := 0; i < 3; i++ {
		if i == len(data.Results) {
			break
		}
		resultItem = append(resultItem, model.ResultItem{
			Title: data.Results[i].Title,
			Url:   data.Results[i].Url,
		})
	}
	return
}

func CallSearchCrawlerTextApi(text string) (err error, isValid bool, resultItem []model.ResultItem) {
	isValid = true
	url := "http://3.1.64.2/newssrc2?query=" + ProcessText(text)

	resp, err := http.Get(url)

	if err != nil {
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var data = new(model.NewsSearchResult)
	err = json.Unmarshal(body, &data)
	if err != nil {
		fmt.Println("whoops:", err)
	}
	fmt.Println(data.Query)
	fmt.Printf(" URL IS %s ", url)
	fmt.Printf("HMMM %v", data)

	if len(data.Results) < 1 {
		isValid = false
		return
	}

	for i := 0; i < 3; i++ {
		if i == len(data.Results) {
			break
		}
		resultItem = append(resultItem, model.ResultItem{
			Title: data.Results[i].Title,
			Url:   data.Results[i].Url,
		})
	}

	return
}

func GenerateTemplateMessage(detailInfo string, resultItem []model.ResultItem) *linebot.BubbleContainer {

	var listQuestion []linebot.FlexComponent

	for i := 0; i < len(resultItem); i++ {
		var title = resultItem[i].Title
		var url = resultItem[i].Url
		listQuestion = append(listQuestion,
			&linebot.ButtonComponent{
				Type:   linebot.FlexComponentTypeButton,
				Style:  linebot.FlexButtonStyleTypeLink,
				Height: linebot.FlexButtonHeightTypeSm,
				Action: linebot.NewURIAction(title[:39], url),
			},
		)
	}

	container := &linebot.BubbleContainer{
		Type: linebot.FlexContainerTypeBubble,
		Body: &linebot.BoxComponent{
			Type:   linebot.FlexComponentTypeBox,
			Layout: linebot.FlexBoxLayoutTypeVertical,
			Contents: []linebot.FlexComponent{
				&linebot.TextComponent{
					Type: linebot.FlexComponentTypeText,
					Text: detailInfo,
					Wrap: true,
				},
			},
		},
		Footer: &linebot.BoxComponent{
			Type:     linebot.FlexComponentTypeBox,
			Layout:   linebot.FlexBoxLayoutTypeVertical,
			Contents: listQuestion,
		},
	}
	return container
}

func GenerateTemplateImageMap() *linebot.ImagemapMessage {
	return nil
}

func GenerateTemplateFlexMap(detailInfo string, url1 string, title1 string, url2 string, title2 string, url3 string, title3 string) (container *linebot.BubbleContainer) {
	container = &linebot.BubbleContainer{
		Type: linebot.FlexContainerTypeBubble,
		Body: &linebot.BoxComponent{
			Type:   linebot.FlexComponentTypeBox,
			Layout: linebot.FlexBoxLayoutTypeVertical,
			Contents: []linebot.FlexComponent{
				&linebot.TextComponent{
					Type: linebot.FlexComponentTypeText,
					Text: detailInfo,
					Wrap: true,
				},
			},
		},
	}
	return
}
