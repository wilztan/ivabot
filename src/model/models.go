package model

type CrawlResult struct {
	CrawlUrl string       `json:"crawl_url"`
	Results  []ResultItem `json:"results"`
}

type ResultItem struct {
	Title string `json:"title"`
	Url   string `json:"Url"`
}

type NewsSearchResult struct {
	Query   string         `json:"query"`
	Results []ResultSearch `json:"results"`
}

type ResultSearch struct {
	Title   string `json:"title"`
	Url     string `json:"url"`
	Desc    string `json:"desc"`
	Website string `json:"website"`
	Source  string `json:"source"`
	Date    string `json:"date"`
	Image   string `json:"image"`
}
